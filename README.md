# Projet cadences en Spring Boot

## Paramétrage Intellij 

https://stackoverflow.com/questions/43129647/intellij-idea-spring-boot-hot-reload-on-manual-save

Pour activer le "hot reload" en développement.

## Accéder à la base de données

http://localhost:8080/h2-console

url : jdbc:h2:file:./local-db.h2
user : sa
mdp : 

Possibilité de configurer l'accès depuis IntelliJ via l'onget database (utiliser les mêmes paramètres, type de datasource : H2)

## Démonstration d'usage de JPA :

Observer le contenu de la classe `CadencesApplicationTests`.
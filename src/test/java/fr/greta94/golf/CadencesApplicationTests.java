package fr.greta94.golf;

import fr.greta94.golf.models.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class CadencesApplicationTests {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private GolfRepository golfRepository;

    @Autowired
    private HoleRepository holeRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void shouldPerformQuery() {
        assertThat(courseRepository.count()).isEqualTo(0);
    }

    @Test
    void shouldCreateGolfEntity() {
        assertThat(golfRepository.count()).isEqualTo(0);
        assertThat(courseRepository.count()).isEqualTo(0);



        Golf golf = new Golf("Cély en Bière");
        assertThat(golf.getId()).isNull(); // l'id est null

        golfRepository.save(golf);

        assertThat(golfRepository.count()).isEqualTo(1);
        assertThat(golf.getId()).isNotNull(); // après sauvegarde en base, nous avons l'id sur l'objet




        Course newCourse = new Course("Parcours indépendant");
        newCourse.setGolf(golf);
        courseRepository.save(newCourse);
        assertThat(newCourse.getId()).isNotNull();
        assertThat(courseRepository.count()).isEqualTo(1);

        Golf golfFromDatabase = golfRepository.findById(golf.getId()).get();
        assertThat(golfFromDatabase.getCourses().size()).isEqualTo(1);
    }

}
